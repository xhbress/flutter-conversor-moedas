import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

const url = 'https://api.hgbrasil.com/finance?format=json-cors&key=9c16fbea';

main() async {
  runApp(MaterialApp(
    title: '',
    home: Conversor(),
    theme: ThemeData(hintColor: Colors.white, primaryColor: Colors.white),
  ));
}

Future<Map> getData() async {
  http.Response response = await http.get(url);
  return json.decode(response.body);
}

class Conversor extends StatefulWidget {
  @override
  _ConversorState createState() => _ConversorState();
}

class _ConversorState extends State<Conversor> {
  final realController = TextEditingController();
  final dolarController = TextEditingController();
  final euroController = TextEditingController();

  double euro;
  double dolar;

  _realChanged(String text) {
    double real = double.parse(text);
    dolarController.text = (real / dolar).toStringAsFixed(2);
    euroController.text = (real / euro).toStringAsFixed(2);
  }

  _dolarChanged(String text) {
    double dolar = double.parse(text);
    realController.text = (dolar * this.dolar).toStringAsFixed(2);
    euroController.text = (euro * this.euro / dolar).toStringAsFixed(2);
  }

  _euroChanged(String text) {
    double euro = double.parse(text);
    realController.text = (euro * this.euro).toStringAsFixed(2);
    dolarController.text = (euro * this.euro / dolar).toStringAsFixed(2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Conversor de Moedas",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        backgroundColor: Colors.purple[600],
        elevation: 0,
      ),
      backgroundColor: Colors.purple[600],
      body: FutureBuilder<Map>(
        future: getData(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Center(
                child: Text("Buscando dados, aguarde...",
                    style: TextStyle(color: Colors.white, fontSize: 25.0)),
              );
            default:
              if (snapshot.hasError) {
                return Center(
                    child: Text("Ocorreu um erro ao buscar os dados :(",
                        style: TextStyle(color: Colors.white, fontSize: 25.0)));
              } else {
                dolar = snapshot.data["results"]["currencies"]["USD"]["buy"];
                euro = snapshot.data["results"]["currencies"]["EUR"]["buy"];
                return SingleChildScrollView(
                    padding: EdgeInsets.all(20.00),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Icon(Icons.monetization_on,
                            size: 140.0, color: Colors.white),
                        Padding(padding: EdgeInsets.only(top: 20.00)),
                        buildTextField(
                            "Reais", "R\$", realController, _realChanged),
                        Padding(padding: EdgeInsets.only(top: 20.00)),
                        buildTextField(
                            "Dólares", "US\$", dolarController, _dolarChanged),
                        Padding(padding: EdgeInsets.only(top: 20.00)),
                        buildTextField(
                            "Euros", " €", euroController, _euroChanged)
                      ],
                    ));
              }
          }
        },
      ),
    );
  }
}

Widget buildTextField(String label, String prefix,
    TextEditingController controller, Function function) {
  return TextField(
    onChanged: function,
    controller: controller,
    keyboardType: TextInputType.number,
    decoration: InputDecoration(
        labelText: label,
        labelStyle: TextStyle(color: Colors.white),
        border: OutlineInputBorder(),
        prefixText: prefix),
    style: TextStyle(color: Colors.white, fontSize: 20.0),
  );
}
